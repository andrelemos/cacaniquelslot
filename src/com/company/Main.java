package com.company;

// cada slot deve conter: banana, framboesa, moeda e sete
// deve ter 3 slots
// se repetir 3 símbolos a pontuação é multiplicada por 100
// cada execução deve sortear a máquina uma vez e exibir o resultado
// a máquina deve exibir no console os valores de cada slot e a pontuação final
// a máquina pode sofrer alterações na quantidade e tipo dos símbolos e na quantidade de slots, portanto
// é desejável uma modelagem que auxilie esse tipo de modificação

import java.util.ArrayList;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Sorteio sorteio = new Sorteio();
        Maquina maquina = new Maquina();

        sorteio.sortearValor();
        sorteio.getSorteados();
        sorteio.getSoma();
        maquina.validarBonus(sorteio.sorteados, sorteio.soma);



    }
}
