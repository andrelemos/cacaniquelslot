package com.company;

import java.util.ArrayList;
import java.util.Random;

public class Sorteio {

    ArrayList<Slots> sorteados = new ArrayList<>();
    Random random = new Random();
    int soma = 0;

    public ArrayList<Slots> getSorteados() {
        return sorteados;
    }

    public int getSoma() {
        return soma;
    }

    public void sortearValor() {

        for (int i = 0; i < 3; i++) {
            int numAleatorio = random.nextInt(Slots.values().length);
            sorteados.add(Slots.values()[numAleatorio]);
            soma += Slots.values()[numAleatorio].getValue();
//            System.out.println(Slots.values()[numAleatorio] + "  " + Slots.values()[numAleatorio].getValue());
        }
//        System.out.println(sorteados + " soma: " + soma);
    }
}


