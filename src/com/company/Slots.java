package com.company;

public enum Slots {

    BANANA(10),
    FRAMBOESA(50),
    MOEDA(100),
    SETE(300),
    ;

    private int value;

    public int getValue() {
        return value;
    }

    Slots(int value) {
        this.value = value;
    }

    public int getNum() {
        return value;
    }
}

